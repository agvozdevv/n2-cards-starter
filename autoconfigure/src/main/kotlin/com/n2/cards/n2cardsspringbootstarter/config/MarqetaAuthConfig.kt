package com.n2.cards.n2cardsspringbootstarter.config

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions

@Configuration
@ConditionalOnProperty(prefix = "marqeta.api.auth", name = ["user", "password"])
class MarqetaAuthConfig(
    @Value("\${marqeta.api.auth.user}")
    private val marqetaUserName: String,

    @Value("\${marqeta.api.auth.password}")
    private val marqetaPassword: String
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @Bean
    @ConditionalOnMissingBean
    fun marqetaAuthFilter(): ExchangeFilterFunction {
        logger.info("Adding marqeta exchange filter function to context...")
        return ExchangeFilterFunctions.basicAuthentication(marqetaUserName, marqetaPassword)
    }
}
