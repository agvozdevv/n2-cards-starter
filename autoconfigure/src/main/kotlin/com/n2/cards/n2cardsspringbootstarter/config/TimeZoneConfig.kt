package com.n2.cards.n2cardsspringbootstarter.config

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import java.util.TimeZone
import javax.annotation.PostConstruct

@Configuration
class TimeZoneConfig {
    private val logger = LoggerFactory.getLogger(javaClass)

    @PostConstruct
    fun initTimeZone() {
        logger.info("Configuring custom time zone...")
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }
}
